## Sorting-Visualizer

I wanted to understand how different types of sorting algorithms work, and at that time I thought that the best way for me to understand something is to create a project out of it. So, I made this, which is based off of this tutorial https://www.youtube.com/watch?v=pFXYym4Wbkc.



You can play around with the sorting visualizer by clicking on this link: [https://manullang.c.gitlab.io/sorting-visualizer ](https://manullang.c.gitlab.io/sorting-visualizer)