export function getHeapSortAnimations(array){
    const animations = [];
    heapSortHelper(array, animations);
    return [array, animations];
}
    
function heapSortHelper(array, animations){
    //Array starts out as full.
    //As the sorting algorithm extracts an element, it will decrease;
    let arrayLength = array.length;
    buildMaxHeap(array, arrayLength, animations);
    //One by one, extract an element from the heap
    for (let idx = array.length - 1; idx > 0; idx--){
        //These are the values that we're swapping; we push them once
        //to change their color
        [array[0], array[idx]] = [array[idx], array[0]];
        animations.push(["swap", 0, array[0], idx, array[idx]]);
        //These are the values that we're swapping; we push them once more
        //to revert their color
        animations.push(["revert", 0, idx]);
        arrayLength--;
        heapify(array, arrayLength, 0, animations);
    }
}

function heapify(array,
    arrayLength,
    idx,
    animations){
    let left = 2 * idx + 1;
    let right = 2 * idx + 2;
    let max = idx;
    if (left < arrayLength && array[left] > array[max]){
        //These are the values that we're comparing; we push them once
        //to change their color
        animations.push(["compare", left, max]);
        //We push them once more to revert their color
        animations.push(["revert", left, max]);
        max = left;
    }
    if (right < arrayLength && array[right] > array[max]){
        //These are the values that we're comparing; we push them once
        //to change their color
        animations.push(["compare", right, max]);
        //These are the values that we're comparing; we push them once more
        //to revert their color
        animations.push(["revert", right, max]);
        max = right;
    }
    if (max !== idx){
        [array[idx], array[max]] = [array[max], array[idx]];
        //These are the values that we're swapping; we push them once
        //To change their color
        animations.push(["swap", idx, array[idx], max, array[max]]);
        //These are the values that we're swapping; we push them once more
        //To revert their color
        animations.push(["revert", idx, max]);
        //Recursively Heapify the affected sub-tree
        heapify(array, arrayLength, max, animations);
    }
}

//Heapify in the bottom order;
function buildMaxHeap(array,
    arrayLength,
    animations ){
    for (let idx = Math.floor(arrayLength/2) - 1; idx >= 0; idx--){
        heapify(array, arrayLength, idx, animations);
    }
}