export function getQuickSortAnimations(array){
    const animations = [];
    if (array.length <= 1) return array;
    let startIdx = 0;
    let endIdx = array.length - 1;
    quickSortHelper(array, startIdx, endIdx, animations);
    return [array, animations];
}
//This quicksort algorithm uses the end as the first pivot
function quickSortHelper(
    array,
    startIdx, 
    endIdx,
    animations
) {
    if (startIdx < endIdx)
    {
        //We determine where the index that becomes the divider of the array is located.
        let partitionIdx = partition(array, startIdx, endIdx, animations);
        //After we have determined the divider's index, we process each partition separately.
        quickSortHelper(array, startIdx, partitionIdx - 1, animations);
        quickSortHelper(array, partitionIdx + 1, endIdx, animations); 
    }
}

function partition(
    array,
    startIdx,
    endIdx,
    animations
) {
    //We use the end of the partition as the pivot
    let pivot = array[endIdx];
    //We push the pivot once to change its color
    animations.push(["pivot", endIdx]);
    let i = (startIdx - 1); //Index of smaller element
    for (let j = startIdx; j <= endIdx - 1; j++){
        //We are now trying to compare the pivot with currentElement
        //We push the currentElement to change its color
        animations.push(["comparedToPivot", i]);
        if (array[j] < pivot){
            i++; 
            [array[i], array[j]] = [array[j], array[i]];
            //We push i and j to change the color to animate the swap
            animations.push(["swap", i, array[i], j, array[j]]);
            //We push i and j to revert its color
            animations.push(["revert", i, j]);
        }
    }
    //We now revert the color of the pivot
    animations.push(["revert", endIdx]);
    [array[i + 1], array[endIdx]] = [array[endIdx], array[i + 1]]; //Swap pivot position with endIdx
    //These are the values that we're swapping; we push them once
    //to change their color
    animations.push(["swap", i + 1, array[i + 1], endIdx, array[endIdx]]);
    //These are the values that we're swapping; we push them once more
    //to revert their color
    animations.push(["revert", i + 1, endIdx]);
    return (i + 1);
}

