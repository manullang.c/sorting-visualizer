export function getBubbleSortAnimations(array){
    const animations = [];
    bubbleSortHelper(array, animations);
    return [array, animations];
}

function bubbleSortHelper(array, animations){
    let n = array.length - 1;
    for (let i = 0; i < n; i++){
        let isSwapOccur = false;
        for (let j = 0; j < n; j++){
            //These are the values that we're comparing; we push them once
            //to change their color
            animations.push(["compare", j, j + 1]);
            if (array[j] > array[j + 1]){
                [array[j], array[j + 1]] = [array[j + 1], array[j]];
                //These are the values that we're swapping; we push them once
                //to change their color
                animations.push(["swap", j, array[j], j + 1, array[j + 1]]);
                //These are the values that we're swapping; we push them once more
                //to revert their color
                animations.push(["revert", j, j + 1]);
                //We now indicate that a swap has occured. This means that
                //in this iteration the array has not been completely sorted yet.
                isSwapOccur = true;
            }
            //These are the values that we're comparing; we push them once more
            //to revert their color
            animations.push(["revert", j, j + 1]);
        }
        //If a swap did not occur in this iteration, it means that the array is already sorted.
        //Therefore we do not need to do another iteration, and the sorting is complete.
        if (!isSwapOccur){
            break;
        }
    }
    return array;
}
