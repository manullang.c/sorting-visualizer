import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {getMergeSortAnimations} from './sortingAlgorithms/mergeSort.js';
import {getQuickSortAnimations} from './sortingAlgorithms/quickSort.js';
import {getHeapSortAnimations} from './sortingAlgorithms/heapSort.js';
import {getBubbleSortAnimations} from './sortingAlgorithms/bubbleSort.js';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('all algos process the array correctly', testSortingAlgorithms);

function testSortingAlgorithms() {
  for (let i = 0; i < 100; i++) {
    const array = [];
    const length = randomIntFromInterval(1, 1000);
    for (let i = 0; i < length; i++) {
      array.push(randomIntFromInterval(-1000, 1000));
    }
    const javaScriptSortedArray = array.slice().sort((a, b) => a - b);
    const mergeSortedArray = getMergeSortAnimations(array.slice())[0];
    const heapSortedArray = getHeapSortAnimations(array.slice())[0];
    const quickSortedArray = getQuickSortAnimations(array.slice())[0];
    const bubbleSortedArray = getBubbleSortAnimations(array.slice())[0];
    expect(JSON.stringify(javaScriptSortedArray)).toMatch(JSON.stringify(mergeSortedArray));
    expect(JSON.stringify(javaScriptSortedArray)).toMatch(JSON.stringify(heapSortedArray));
    expect(JSON.stringify(javaScriptSortedArray)).toMatch(JSON.stringify(quickSortedArray));
    expect(JSON.stringify(javaScriptSortedArray)).toMatch(JSON.stringify(bubbleSortedArray));
  }
}

// From https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript
function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}