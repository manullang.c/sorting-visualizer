import React from 'react';
import {getMergeSortAnimations} from '../sortingAlgorithms/mergeSort.js';
import './SortingVisualizer.css';
import { getQuickSortAnimations } from '../sortingAlgorithms/quickSort.js';
import { getHeapSortAnimations } from '../sortingAlgorithms/heapSort.js';
import { getBubbleSortAnimations} from '../sortingAlgorithms/bubbleSort.js';
import {Navbar, Button} from 'react-bootstrap';

//Body Background
document.body.style = 'background: #28104E;';


// Change this value for the speed of the animations.
const ANIMATION_SPEED_MS = 2;

// Change this value for the number of bars (value) in the array.
const NUMBER_OF_ARRAY_BARS = 200;

// This is the main color of the array bars.
const PRIMARY_COLOR = '#9754CB';

// This is the color of array bars that are being compared throughout the animations.
const SECONDARY_COLOR = '#FFFFFF';
const widthArrayBar = `${200*3/NUMBER_OF_ARRAY_BARS}px`;
export default class SortingVisualizer extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      array: [],
    };
  }

  componentDidMount() {
    this.resetArray();
  }

  resetArray() {
    const array = [];
    for (let i = 0; i < NUMBER_OF_ARRAY_BARS; i++) {
      array.push(randomIntFromInterval(5, 500));
    }
    this.setState({array});
  }

  mergeSort() {
    const animations = getMergeSortAnimations(this.state.array)[1];
    for (let i = 0; i < animations.length; i++) {
      const arrayBars = document.getElementsByClassName('array-bar');
      const isColorChange = i % 3 !== 2;
      if (isColorChange) {
        const [barOneIdx, barTwoIdx] = animations[i];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        const color = i % 3 === 0 ? SECONDARY_COLOR : PRIMARY_COLOR;
        setTimeout(() => {
          barOneStyle.backgroundColor = color;
          barTwoStyle.backgroundColor = color;
        }, i * ANIMATION_SPEED_MS);
      } else {
        setTimeout(() => {
          const [barOneIdx, newHeight] = animations[i];
          const barOneStyle = arrayBars[barOneIdx].style;
          barOneStyle.height = `${newHeight}px`;
        }, i * ANIMATION_SPEED_MS);
      }
    }
    setTimeout(() => {
      "timeoutset"
      this.toggleAllButtons();
    }, animations.length * ANIMATION_SPEED_MS);
  }


  heapSort() {
    // We leave it as an exercise to the viewer of this code to implement this method.
    const animations = getHeapSortAnimations(this.state.array)[1];
    for (let i = 0; i < animations.length; i++){
      const arrayBars = document.getElementsByClassName('array-bar');
      if (animations[i][0] === "compare"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][2];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = SECONDARY_COLOR;
          barTwoStyle.backgroundColor = SECONDARY_COLOR;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "swap"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][3];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = SECONDARY_COLOR;
          barTwoStyle.backgroundColor = SECONDARY_COLOR;
          barOneStyle.height = `${animations[i][2]}px`;
          barTwoStyle.height = `${animations[i][4]}px`;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "revert"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][2];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = PRIMARY_COLOR;
          barTwoStyle.backgroundColor = PRIMARY_COLOR;
        }, i * ANIMATION_SPEED_MS);
      }
    }
    this.setTimeoutToggle(animations.length);
  }

  bubbleSort() {
    // We leave it as an exercise to the viewer of this code to implement this method.
    const animations = getBubbleSortAnimations(this.state.array)[1];
    for (let i = 0; i < animations.length; i++){
      const arrayBars = document.getElementsByClassName('array-bar');
      if (animations[i][0] === "compare"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][2];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = SECONDARY_COLOR;
          barTwoStyle.backgroundColor = SECONDARY_COLOR;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "revert"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][2];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = PRIMARY_COLOR;
          barTwoStyle.backgroundColor = PRIMARY_COLOR;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "swap"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][3];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = SECONDARY_COLOR;
          barTwoStyle.backgroundColor = SECONDARY_COLOR;
          barOneStyle.height = `${animations[i][2]}px`;
          barTwoStyle.height = `${animations[i][4]}px`;
        }, i * ANIMATION_SPEED_MS);
      }
    }
    this.setTimeoutToggle(animations.length);
  }

  quickSort(){
    const getQuick = getQuickSortAnimations(this.state.array);
    const animations = getQuick[1];
    for (let i = 0; i < animations.length; i++){
      const arrayBars = document.getElementsByClassName("array-bar");
      if (animations[i][0] === "pivot"){
        const barPivotIdx = animations[i][1];
        const barPivotStyle = arrayBars[barPivotIdx].style;
        setTimeout(() => {
          barPivotStyle.backgroundColor = SECONDARY_COLOR;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "swap"){
        const barOneIdx = animations[i][1];
        const barTwoIdx = animations[i][3];
        const barOneStyle = arrayBars[barOneIdx].style;
        const barTwoStyle = arrayBars[barTwoIdx].style;
        setTimeout(() => {
          barOneStyle.backgroundColor = SECONDARY_COLOR;
          barTwoStyle.backgroundColor = SECONDARY_COLOR;
          barOneStyle.height = `${animations[i][2]}px`;
          barTwoStyle.height = `${animations[i][4]}px`;
        }, i * ANIMATION_SPEED_MS);
      } else if (animations[i][0] === "revert"){
        if (animations[i].length === 3){
          const barOneIdx = animations[i][1];
          const barTwoIdx = animations[i][2];
          const barOneStyle = arrayBars[barOneIdx].style;
          const barTwoStyle = arrayBars[barTwoIdx].style;
          setTimeout(() => {
            barOneStyle.backgroundColor = PRIMARY_COLOR;
            barTwoStyle.backgroundColor = PRIMARY_COLOR;
          }, i * ANIMATION_SPEED_MS);
        } else if (animations[i].length === 2){
          const barOneIdx = animations[i][1];
          const barOneStyle = arrayBars[barOneIdx].style;
          setTimeout(() => {
            barOneStyle.backgroundColor = PRIMARY_COLOR;
          }, i * ANIMATION_SPEED_MS);
        }
      }
    }
    this.setTimeoutToggle(animations.length);
  }

  toggleAllButtons(){
    var buttons = document.getElementsByTagName('button');
    for (let buttonIdx = 0; buttonIdx < buttons.length; buttonIdx++){
      buttons[buttonIdx].disabled = buttons[buttonIdx].disabled ? false : true;
    }
  }

  setTimeoutToggle(animationLength){
    setTimeout(() => {
      this.toggleAllButtons();
    }, animationLength * ANIMATION_SPEED_MS);
  }
  
  render() {
    const {array} = this.state;

    return (
      <>
      <Navbar bg="" variant="dark">
      <Navbar.Brand>
        Sorting Visualizer
      </Navbar.Brand>
      <Button variant="outline-primary" onClick={() => this.resetArray()}>Generate New Array</Button>
      <Button variant="outline-primary" onClick={() => {this.toggleAllButtons(); this.mergeSort()}}>Merge Sort</Button>
      <Button variant="outline-primary" onClick={() => {this.toggleAllButtons(); this.quickSort();}}>Quick Sort</Button>
      <Button variant="outline-primary" onClick={() => {this.toggleAllButtons(); this.heapSort();}}>Heap Sort</Button>
      <Button variant="outline-primary" onClick={() => {this.toggleAllButtons(); this.bubbleSort()}}>Bubble Sort</Button>
    </Navbar>
      <div className="array-container">
        {array.map((value, idx) => (
          <div
            className="array-bar"
            key={idx}
            style={{
              width: widthArrayBar,
              backgroundColor: PRIMARY_COLOR,
              height: `${value}px`,
            }}></div>
        ))}
      </div></>
    );
  }
}

// From https://stackoverflow.com/questions/4959975/generate-random-number-between-two-numbers-in-javascript
function randomIntFromInterval(min, max) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}
